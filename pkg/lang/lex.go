package lang

import (
	"bufio"
	"io"
	"strings"
)

type lexer struct {
	source string
	line   uint
	col    uint
	tokens []Token
	errs   errorPack
}

// lexString lexes a string starting at start in ctx, and returns the index after the end of the string
func (l *lexer) lexString(start uint, ctx []byte) uint {
	idx := start + 1
	for idx < uint(len(ctx)) && ctx[idx] != '"' {
		idx++
	}

	if ctx[idx] != '"' {
		l.errs = append(l.errs, &errorToken{
			message: "unterminated string literal",
			token:   string(ctx[idx]),
			context: string(ctx),
			source:  l.source,
			pos:     idx,
			line:    l.line,
		})
	} else {
		idx++
		l.tokens = append(l.tokens, Token{
			Type:    TokenString,
			Value:   string(ctx[start+1 : idx-1]), // drop quotes
			Source:  l.source,
			Context: ctx,
			Line:    l.line,
			Col:     start,
		})
	}

	return idx
}

// lexNumber lexes a number starting at start in ctx, and returns the index after the end of the number
func (l *lexer) lexNumber(start uint, ctx []byte) uint {
	hasDot := false
	idx := start

loop:
	for _, b := range ctx[start:] {
		switch b {
		case '1', '2', '3', '4', '5', '6', '7', '8', '9', '0':
			// we're good, nothing to do
			idx++

		case '.':
			// if we already have a dot, then we're finished
			if hasDot {
				break loop
			} else {
				idx++
				hasDot = true
			}

		default:
			// character isn't part of a number, we're done
			break loop
		}
	}

	// we want the index AFTER the end
	//idx++

	l.tokens = append(l.tokens, Token{
		Type:    TokenNumber,
		Value:   string(ctx[start:idx]),
		Source:  l.source,
		Context: ctx,
		Line:    l.line,
		Col:     start,
	})

	return idx
}

// lexIdentifier lexes an identifier starting at start in ctx, and returns the index after the end of the identifier
func (l *lexer) lexIdentifier(start uint, ctx []byte) uint {
	// don't allow starting an identifier with a number
	if isDigit(ctx[start]) {
		l.errs = append(l.errs, &errorToken{
			message: "cannot start an identifier with a number",
			token:   string(ctx[start]),
			context: string(ctx),
			source:  l.source,
			pos:     start,
			line:    l.line,
		})

		return start
	}

	idx := start

loop:
	for _, b := range ctx[start:] {
		if ('a' <= b && b <= 'z') || ('A' <= b && b <= 'Z') || b == '_' || isDigit(b) {
			// valid!
			idx++
		} else {
			// invalid character - we're done
			break loop
		}
	}

	// couldn't actually get anything
	if idx-start == 0 {
		return start
	}

	// check for keyword
	t := Token{
		Value:   strings.TrimSpace(string(ctx[start:idx])),
		Source:  l.source,
		Context: ctx,
		Line:    l.line,
		Col:     start,
	}

	switch t.Value {
	case "true":
		t.Type = TokenTrue
	case "false":
		t.Type = TokenFalse
	case "if":
		t.Type = TokenIf
	case "elseif":
		t.Type = TokenElseIf
	case "else":
		t.Type = TokenElse
	case "while":
		t.Type = TokenWhile
	case "func":
		t.Type = TokenFunc
	default:
		t.Type = TokenIdentifier
	}
	l.tokens = append(l.tokens, t)

	return idx
}

func Lex(source string, r io.Reader) ([]Token, error) {
	l := lexer{
		source: source,
		line:   0,
		col:    0,
		tokens: nil,
		errs:   nil,
	}
	scan := bufio.NewScanner(r)

	for scan.Scan() {
		var (
			line    = scan.Bytes()
			lineLen = uint(len(line))
		)

		ctx := make([]byte, len(line))
		copy(ctx, line)

		for l.col = 0; l.col < lineLen; l.col++ {
			b := line[l.col]

			switch b {
			case ';':
				l.tokens = append(l.tokens, Token{
					Type:    TokenSemicolon,
					Value:   ";",
					Source:  l.source,
					Context: ctx,
					Line:    l.line,
					Col:     l.col,
				})
			case ',':
				l.tokens = append(l.tokens, Token{
					Type:    TokenComma,
					Value:   ",",
					Source:  l.source,
					Context: ctx,
					Line:    l.line,
					Col:     l.col,
				})
			case '(':
				l.tokens = append(l.tokens, Token{
					Type:    TokenParenLeft,
					Value:   "(",
					Source:  l.source,
					Context: ctx,
					Line:    l.line,
					Col:     l.col,
				})
			case ')':
				l.tokens = append(l.tokens, Token{
					Type:    TokenParenRight,
					Value:   ")",
					Source:  l.source,
					Context: ctx,
					Line:    l.line,
					Col:     l.col,
				})
			case '{':
				l.tokens = append(l.tokens, Token{
					Type:    TokenBraceLeft,
					Value:   "{",
					Source:  l.source,
					Context: ctx,
					Line:    l.line,
					Col:     l.col,
				})
			case '}':
				l.tokens = append(l.tokens, Token{
					Type:    TokenBraceRight,
					Value:   "}",
					Source:  l.source,
					Context: ctx,
					Line:    l.line,
					Col:     l.col,
				})
			case '*':
				l.tokens = append(l.tokens, Token{
					Type:    TokenAsterisk,
					Value:   "*",
					Source:  l.source,
					Context: ctx,
					Line:    l.line,
					Col:     l.col,
				})
			case '/':
				l.tokens = append(l.tokens, Token{
					Type:    TokenSlash,
					Value:   "/",
					Source:  l.source,
					Context: ctx,
					Line:    l.line,
					Col:     l.col,
				})
			case '=':
				l.tokens = append(l.tokens, Token{
					Type:    TokenEquals,
					Value:   "=",
					Source:  l.source,
					Context: ctx,
					Line:    l.line,
					Col:     l.col,
				})

			// comment to end of line
			case '#':
				l.col = lineLen

			// until next non-escaped double quote
			case '"':
				l.col = l.lexString(l.col, ctx)

			case '+':
				l.tokens = append(l.tokens, Token{
					Type:    TokenPlus,
					Value:   "+",
					Source:  l.source,
					Context: ctx,
					Line:    l.line,
					Col:     l.col,
				})

			case '-':
				l.tokens = append(l.tokens, Token{
					Type:    TokenMinus,
					Value:   "-",
					Source:  l.source,
					Context: ctx,
					Line:    l.line,
					Col:     l.col,
				})

			case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
				l.col = l.lexNumber(l.col, ctx)
				// our loop is going to increment...
				l.col--

			case ' ', '\t', '\n', '\r':
				// eat whitespace

			default:
				l.col = l.lexIdentifier(l.col, ctx)
				// our loop is going to increment...
				l.col--
			}
		}
		l.line++
	}

	if err := scan.Err(); err != nil {
		return nil, err
	}

	if l.errs == nil {
		return l.tokens, nil
	} else {
		return l.tokens, l.errs
	}
}

func isDigit(b byte) bool {
	return '0' <= b && b <= '9'
}
