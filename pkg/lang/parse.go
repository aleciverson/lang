package lang

import (
	"errors"
	"fmt"
	"strconv"
)

var (
	unexpectedEndOfFileErr = errors.New("unexpected end of file")
)

type parser struct {
	tokens []Token
	pos    int
	script Script
	errs   errorPack
}

func Parse(tokens []Token) (Script, error) {
	p := parser{
		tokens: tokens,
		pos:    0,
		script: Script{
			Env: Env{
				Values: make(map[string]interface{}),
				Funcs:  make(map[string]Funcer),
				parent: nil,
			},
		},
		errs: nil,
	}

	// add built-in print function
	p.script.Funcs["print"] = BuiltinFunc(func(args ...interface{}) (interface{}, error) {
		_, err := fmt.Println(args...)
		if err != nil {
			return nil, err
		}
		return nil, nil
	})

	for p.pos < len(tokens) {
		p.parseFunc()
	}

	mainFunc, ok := p.script.Funcs["main"]
	if !ok {
		p.errs = append(p.errs, errors.New("script does not have a 'main' func"))
	} else if len(mainFunc.(*Func).Args) > 1 {
		p.errs = append(p.errs, errors.New("'main' must have 0 or 1 arguments"))
	}

	if p.errs != nil {
		return Script{}, p.errs
	}
	return p.script, nil
}

// parseFunc parses a Func from the parser's tokens. any errors are appended to p.errs
func (p *parser) parseFunc() {
	f := Func{}

	t := p.next()
	if t == nil {
		p.errs = append(p.errs, unexpectedEndOfFileErr)
		return
	} else if t.Type != TokenFunc {
		p.errs = append(p.errs, newTokenError("expected 'func' but ", t))
		return
	}

	t = p.next()
	if t == nil {
		p.errs = append(p.errs, unexpectedEndOfFileErr)
		return
	} else if t.Type != TokenIdentifier {
		p.errs = append(p.errs, newTokenError("expected an identifier but got ", t))
		return
	}
	funcName := t.Value

	t = p.next()
	if t == nil {
		p.errs = append(p.errs, unexpectedEndOfFileErr)
		return
	} else if t.Type != TokenParenLeft {
		p.errs = append(p.errs, newTokenError("expected a '(' but got ", t))
		return
	}

	// optional number comma separated args
	t = p.next()
	for t != nil && t.Type != TokenParenRight {
		if t.Type != TokenIdentifier {
			p.errs = append(p.errs, newTokenError("expected an identifier but got ", t))
			p.resetTo(TokenParenRight)
			return
		}
		f.Args = append(f.Args, t.Value)

		t = p.next()
		if t == nil {
			p.errs = append(p.errs, unexpectedEndOfFileErr)
			return
		} else if t.Type == TokenParenRight {
			break
		} else if t.Type != TokenComma {
			p.errs = append(p.errs, newTokenError("expected a ',' but got ", t))
			p.resetTo(TokenParenRight)
			return
		}

		t = p.next()
	}

	if t == nil {
		p.errs = append(p.errs, unexpectedEndOfFileErr)
		return
	}

	f.Expr = p.parseExprGroup()

	if _, ok := p.script.Funcs[funcName]; ok {
		p.errs = append(p.errs, newTokenError("func '"+funcName+"' already defined", t))
	} else {
		p.script.Funcs[funcName] = &f
	}
}

// parseExprGroup parses a series of Expr from the parser's tokens, where the start and end are defined by '{' and '}'. any errors are appended to p.errs
func (p *parser) parseExprGroup() Expr {
	t := p.next()
	if t == nil {
		p.errs = append(p.errs, unexpectedEndOfFileErr)
		return nil
	} else if t.Type != TokenBraceLeft {
		p.errs = append(p.errs, newTokenError("expected a '{' but got ", t))
		return nil
	}

	g := &exprGroup{}
	t = p.next()
	for t != nil && t.Type != TokenBraceRight {
		p.rewind()
		e := p.parseExpr()
		if e == nil {
			p.resetTo(TokenBraceRight)
			return g
		}
		g.Exprs = append(g.Exprs, e)
		t = p.next()
	}

	if t == nil {
		p.errs = append(p.errs, unexpectedEndOfFileErr)
		return nil
	}

	return g
}

// parseExpr parses a Expr from the parser's tokens. any errors are appended to p.errs
func (p *parser) parseExpr() Expr {
	t := p.next()

	switch t.Type {
	case TokenIdentifier:
		tn := p.next()
		if tn.Type == TokenParenLeft {
			return p.parseFuncCall(t)
		}
		p.rewind()
		return exprVar(t.Value)

	case TokenPlus:
		return p.parseAdd()

	case TokenMinus:
		return p.parseSub()

	case TokenAsterisk:
		return p.parseMultiply()

	case TokenSlash:
		return p.parseDivide()

	case TokenEquals:
		return p.parseAssign()

	case TokenIf:
		return p.parseIf()

	case TokenWhile:
		return p.parseWhile()

	case TokenString:
		return exprString(t.Value)

	case TokenNumber:
		v, err := strconv.ParseFloat(t.Value, 64)
		if err != nil {
			p.errs = append(p.errs, newTokenError(err.Error(), t))
		}
		return exprNumber(v)

	case TokenTrue:
		return exprBool(true)

	case TokenFalse:
		return exprBool(false)

	default:
		p.errs = append(p.errs, newTokenError("expected beginning of new expression", t))
		return nil
	}
}

func (p *parser) parseFuncCall(funcIdentifier *Token) Expr {
	fc := &exprFuncCall{
		Func: funcIdentifier.Value,
	}

	t := &Token{
		Type: TokenParenLeft,
	}
	for t != nil && t.Type != TokenParenRight {
		fc.Args = append(fc.Args, p.parseExpr())

		t = p.next()
		if t == nil {
			p.errs = append(p.errs, unexpectedEndOfFileErr)
			return nil
		} else if t.Type == TokenParenRight {
			break
		} else if t.Type != TokenComma {
			p.errs = append(p.errs, newTokenError("expected a ',' but got ", t))
			p.resetTo(TokenParenRight)
			return nil
		}
	}

	if t == nil {
		p.errs = append(p.errs, unexpectedEndOfFileErr)
		return nil
	}

	return fc
}

func (p *parser) parseAdd() Expr {
	l := p.parseExpr()
	r := p.parseExpr()
	return &exprAdd{
		Left:  l,
		Right: r,
	}
}

func (p *parser) parseSub() Expr {
	l := p.parseExpr()
	r := p.parseExpr()
	return &exprSub{
		Left:  l,
		Right: r,
	}
}

func (p *parser) parseMultiply() Expr {
	l := p.parseExpr()
	r := p.parseExpr()
	return &exprMultiply{
		Left:  l,
		Right: r,
	}
}

func (p *parser) parseDivide() Expr {
	l := p.parseExpr()
	r := p.parseExpr()
	return &exprDivide{
		Left:  l,
		Right: r,
	}
}

func (p *parser) parseAssign() Expr {
	t := p.next()
	if t.Type != TokenIdentifier {
		return nil
	}
	return &exprAssign{
		Left:  t.Value,
		Right: p.parseExpr(),
	}
}

func (p *parser) parseIf() Expr {
	c := p.parseExpr()
	if c == nil {
		return nil
	}
	ifTrue := p.parseExprGroup()
	if ifTrue == nil {
		return nil
	}

	t := p.next()
	if t == nil {
		p.errs = append(p.errs, unexpectedEndOfFileErr)
		return nil
	}

	var elseIfs []conditionalExpr
	for t != nil && t.Type == TokenElseIf {
		ei := p.parseElseIf()
		elseIfs = append(elseIfs, ei)
		t = p.next()
	}

	if t == nil {
		p.errs = append(p.errs, unexpectedEndOfFileErr)
		return nil
	}

	var els Expr
	if t.Type == TokenElse {
		els = p.parseElse()
	}

	return &exprIf{
		Condition: c,
		True:      ifTrue,
		Other:     elseIfs,
		False:     els,
	}
}

func (p *parser) parseElseIf() conditionalExpr {
	c := p.parseExpr()
	if c == nil {
		p.errs = append(p.errs, unexpectedEndOfFileErr)
		return conditionalExpr{}
	}

	e := p.parseExprGroup()
	if e == nil {
		p.errs = append(p.errs, unexpectedEndOfFileErr)
		return conditionalExpr{}
	}

	return conditionalExpr{
		Condition: c,
		Expr:      e,
	}
}

func (p *parser) parseElse() Expr {
	return p.parseExprGroup()
}

func (p *parser) parseWhile() Expr {
	c := p.parseExpr()
	if c == nil {
		p.errs = append(p.errs, unexpectedEndOfFileErr)
		return nil
	}

	e := p.parseExprGroup()
	if e == nil {
		p.errs = append(p.errs, unexpectedEndOfFileErr)
		return nil
	}

	return &exprWhile{
		Condition: c,
		Expr:      e,
	}
}

func (p *parser) next() *Token {
	if p.pos < len(p.tokens) {
		t := &p.tokens[p.pos]
		p.pos++
		return t
	}
	return nil
}

// rewind moves the token index back 1
func (p *parser) rewind() {
	p.pos--
}

// resetTo attempts to reset the parser to a valid state by skipping tokens until a instance of tt is reached (which will be consumed as well)
func (p *parser) resetTo(tt TokenType) {
	t := p.next()
	for t != nil && t.Type != tt {
		t = p.next()
	}
}
