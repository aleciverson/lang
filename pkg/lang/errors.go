package lang

import (
	"bytes"
	"strconv"
)

type errorToken struct {
	message string
	token   string
	context string
	source  string
	pos     uint
	line    uint
}

func newTokenError(message string, t *Token) error {
	return &errorToken{
		message: message,
		token:   t.Value,
		context: string(t.Context),
		source:  t.Source,
		pos:     t.Col,
		line:    t.Line,
	}
}

func (e *errorToken) Error() string {
	buf := bytes.NewBuffer(nil)
	buf.WriteString(e.source)
	buf.WriteByte(':')
	buf.WriteString(strconv.FormatUint(uint64(e.line+1), 10))
	buf.WriteByte(':')
	buf.WriteString(strconv.FormatUint(uint64(e.pos+1), 10))
	buf.WriteString(" - ")
	buf.WriteString(e.message)
	buf.WriteByte('\'')
	buf.WriteString(e.token)
	buf.WriteString("'\n  ")
	buf.WriteString(e.context)
	buf.WriteString("\n  ")
	for i := uint(0); i < e.pos; i++ {
		buf.WriteByte(' ')
	}
	buf.WriteByte('^')
	return buf.String()
}

type errorPack []error

func (e errorPack) Error() string {
	buf := bytes.NewBuffer(nil)
	for _, err := range e {
		buf.WriteString(err.Error())
		buf.WriteByte('\n')
	}
	return buf.String()
}
