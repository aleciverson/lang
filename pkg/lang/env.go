package lang

import "fmt"

type Env struct {
	Values map[string]interface{}
	Funcs  map[string]Funcer
	parent *Env
}

func NewEnv(parent *Env) *Env {
	return &Env{
		Values: make(map[string]interface{}),
		parent: parent,
	}
}

func (e *Env) Set(identifier string, value interface{}) {
	e.Values[identifier] = value
}

func (e *Env) Get(identifier string) interface{} {
	p := e
	for p != nil {
		v, ok := e.Values[identifier]
		if ok {
			return v
		}
		p = p.parent
	}
	return nil
}

func (e *Env) Has(identifier string) bool {
	p := e
	for p != nil {
		_, ok := e.Values[identifier]
		if ok {
			return true
		}
		p = p.parent
	}
	return false
}

func (e *Env) Call(funcName string, args ...Expr) (interface{}, error) {
	var (
		p  = e
		ok = false
		f  Funcer
	)

	for p != nil && !ok {
		f, ok = p.Funcs[funcName]
	}
	if !ok {
		return nil, fmt.Errorf("bad function call '%s': does not exist", funcName)
	}

	res, err := f.Call(e, args...)
	if err != nil {
		return nil, fmt.Errorf("error calling function: '%s': %v", funcName, err)
	}
	return res, nil
}
