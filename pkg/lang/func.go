package lang

import (
	"bytes"
	"fmt"
)

type Funcer interface {
	Call(e *Env, args ...Expr) (interface{}, error)
}

type Func struct {
	Expr Expr
	Args []string
}

func (f *Func) Call(e *Env, args ...Expr) (interface{}, error) {
	if len(args) != len(f.Args) {
		return nil, fmt.Errorf("incorrect number of arguments (expected %d, was %d)", len(args), len(f.Args))
	}

	fe := NewEnv(nil)
	fe.Funcs = e.Funcs
	for i := 0; i < len(f.Args); i++ {
		fe.Set(f.Args[i], args[i].Eval(e))
	}

	return f.Expr.Eval(fe), nil
}

func (f *Func) String() string {
	buf := bytes.NewBuffer(nil)

	buf.WriteString("func (")

	if len(f.Args) > 0 {
		buf.WriteString(f.Args[0])
		for _, a := range f.Args[1:] {
			buf.WriteString(", ")
			buf.WriteString(a)
		}
	}

	buf.WriteString(")\n")
	buf.WriteString(f.Expr.String(1))

	return buf.String()
}

type BuiltinFunc func(...interface{}) (interface{}, error)

func (b BuiltinFunc) Call(e *Env, args ...Expr) (interface{}, error) {
	argVals := make([]interface{}, 0, len(args))
	for _, a := range args {
		argVals = append(argVals, a.Eval(e))
	}
	return b(argVals...)
}
