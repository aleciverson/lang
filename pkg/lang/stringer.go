package lang

type Stringer interface {
	String(indentLevel int) string
}
