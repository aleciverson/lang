package lang

type TokenType uint

const (
	TokenIdentifier TokenType = iota
	TokenString
	TokenNumber
	TokenTrue
	TokenFalse
	TokenIf
	TokenElseIf
	TokenElse
	TokenWhile
	TokenFunc
	TokenComma
	TokenSemicolon
	TokenParenLeft
	TokenParenRight
	TokenBraceLeft
	TokenBraceRight
	TokenPlus
	TokenMinus
	TokenAsterisk
	TokenSlash
	TokenEquals
	TokenNewline
)

type Token struct {
	Type    TokenType
	Value   string
	Source  string
	Context []byte
	Line    uint
	Col     uint
}
