package lang

import (
	"bytes"
	"errors"
	"strconv"
	"strings"
)

type IdentedStringer interface {
	String(indentLevel int) string
}

type Expr interface {
	Eval(e *Env) interface{}
	IdentedStringer
}

func Truthy(v interface{}) bool {
	switch rv := v.(type) {
	case string:
		return rv != ""
	case float64:
		return rv != 0.0
	case bool:
		return rv
	case error:
		return rv == nil
	default:
		return false
	}
}

type exprString string

func (s exprString) Eval(*Env) interface{} {
	return string(s)
}

func (s exprString) String(indentLevel int) string {
	return strings.Repeat("\t", indentLevel) + strconv.Quote(string(s))
}

type exprNumber float64

func (n exprNumber) Eval(*Env) interface{} {
	return float64(n)
}

func (n exprNumber) String(indentLevel int) string {
	return strings.Repeat("\t", indentLevel) + strconv.FormatFloat(float64(n), 'f', 4, 64)
}

type exprBool bool

func (b exprBool) Eval(*Env) interface{} {
	return bool(b)
}

func (b exprBool) String(indentLevel int) string {
	if b {
		return strings.Repeat("\t", indentLevel) + "true"
	} else {
		return strings.Repeat("\t", indentLevel) + "false"
	}
}

type exprVar string

func (v exprVar) Eval(e *Env) interface{} {
	return e.Get(string(v))
}

func (v exprVar) String(indentLevel int) string {
	return strings.Repeat("\t", indentLevel) + string(v)
}

type conditionalExpr struct {
	Condition Expr
	Expr      Expr
}

type exprIf struct {
	Condition Expr
	True      Expr
	Other     []conditionalExpr
	False     Expr
}

func (i *exprIf) Eval(e *Env) interface{} {
	// if...
	if Truthy(i.Condition.Eval(e)) {
		return i.True.Eval(e)
	}

	// elseif...
	for _, c := range i.Other {
		if Truthy(c.Condition.Eval(e)) {
			return c.Expr.Eval(e)
		}
	}

	// else...
	if i.False != nil {
		return i.False.Eval(e)
	} else {
		return exprBool(false)
	}
}

func (i *exprIf) String(indentLevel int) string {
	buf := bytes.NewBuffer(nil)

	buf.WriteString(strings.Repeat("\t", indentLevel))
	buf.WriteString("If:\n")

	buf.WriteString(strings.Repeat("\t", indentLevel+1))
	buf.WriteString("Condition:\n")
	buf.WriteString(i.Condition.String(indentLevel + 2))
	buf.WriteByte('\n')

	buf.WriteString(strings.Repeat("\t", indentLevel+1))
	buf.WriteString("Expr:\n")
	buf.WriteString(i.True.String(indentLevel + 2))
	buf.WriteByte('\n')

	for _, c := range i.Other {
		buf.WriteString(strings.Repeat("\t", indentLevel))
		buf.WriteString("ElseIf:\n")

		buf.WriteString(strings.Repeat("\t", indentLevel+1))
		buf.WriteString("Condition:\n")
		buf.WriteString(c.Condition.String(indentLevel + 2))
		buf.WriteByte('\n')

		buf.WriteString(strings.Repeat("\t", indentLevel+1))
		buf.WriteString("Expr:\n")
		buf.WriteString(c.Expr.String(indentLevel + 2))
		buf.WriteByte('\n')
	}

	buf.WriteString(strings.Repeat("\t", indentLevel))
	buf.WriteString("Else:\n")
	buf.WriteString(strings.Repeat("\t", indentLevel+1))
	buf.WriteString("Expr:\n")
	buf.WriteString(i.False.String(indentLevel + 2))

	return buf.String()
}

type exprWhile struct {
	Condition Expr
	Expr      Expr
}

func (w *exprWhile) Eval(e *Env) interface{} {
	var ret interface{}
	for Truthy(w.Condition.Eval(e)) {
		ret = w.Expr.Eval(e)
	}
	return ret
}

func (w *exprWhile) String(indentLevel int) string {
	buf := bytes.NewBuffer(nil)

	buf.WriteString(strings.Repeat("\t", indentLevel))
	buf.WriteString("While:\n")

	buf.WriteString(strings.Repeat("\t", indentLevel+1))
	buf.WriteString("Condition:\n")
	buf.WriteString(w.Condition.String(indentLevel + 2))

	buf.WriteByte('\n')

	buf.WriteString(strings.Repeat("\t", indentLevel+1))
	buf.WriteString("Expr:\n")
	buf.WriteString(w.Expr.String(indentLevel + 2))

	return buf.String()
}

type exprFuncCall struct {
	Func string
	Args []Expr
}

func (c *exprFuncCall) Eval(e *Env) interface{} {
	ret, err := e.Call(c.Func, c.Args...)
	if err != nil {
		return err
	}
	return ret
}

func (c *exprFuncCall) String(indentLevel int) string {
	buf := bytes.NewBuffer(nil)

	prefix := strings.Repeat("\t", indentLevel)

	buf.WriteString(prefix)
	buf.WriteString("Func call: ")
	buf.WriteString(c.Func)
	buf.WriteByte('\n')

	if len(c.Args) > 0 {
		buf.WriteString(c.Args[0].String(indentLevel + 1))
		buf.WriteByte('\n')

		for _, a := range c.Args[1:] {
			buf.WriteString(a.String(indentLevel + 1))
			buf.WriteByte('\n')
		}
	}

	return buf.String()
}

type exprGroup struct {
	Exprs []Expr
}

func (g *exprGroup) Eval(e *Env) interface{} {
	l := len(g.Exprs)
	for _, ex := range g.Exprs[:l-1] {
		ex.Eval(e)
	}
	return g.Exprs[l-1].Eval(e)
}

func (g *exprGroup) String(indentLevel int) string {
	buf := bytes.NewBuffer(nil)

	for _, e := range g.Exprs {
		buf.WriteString(e.String(indentLevel))
		buf.WriteByte('\n')
	}

	return buf.String()
}

type exprAdd struct {
	Left  Expr
	Right Expr
}

func (a *exprAdd) Eval(e *Env) interface{} {
	lhs := a.Left.Eval(e)
	rhs := a.Right.Eval(e)

	switch lv := lhs.(type) {
	case float64:
		switch rv := rhs.(type) {
		case float64:
			return lv + rv

		case string:
			return strconv.FormatFloat(lv, 'f', 4, 64) + rv

		case bool:
			return errors.New("cannot add number and bool")

		case error:
			return errors.New("cannot add number and error")

		default:
			return errors.New("could not determine type of right side")
		}

	case string:
		switch rv := rhs.(type) {
		case float64:
			return lv + strconv.FormatFloat(rv, 'f', 4, 64)

		case string:
			return lv + rv

		case bool:
			if rv {
				return lv + "true"
			} else {
				return lv + "false"
			}

		case error:
			if rv != nil {
				return lv + rv.Error()
			} else {
				return lv + "none"
			}

		default:
			return errors.New("could not determine type of right side")
		}

	case bool:
		switch rv := rhs.(type) {
		case float64:
			return errors.New("cannot add bool and number")

		case string:
			if lv {
				return "true" + rv
			} else {
				return "false" + rv
			}

		case bool:
			return lv || rv

		case error:
			return lv || rv == nil

		default:
			return errors.New("could not determine type of right side")
		}

	case error:
		switch rv := rhs.(type) {
		case float64:
			return errors.New("cannot add error and number")

		case string:
			if lv != nil {
				return lv.Error() + rv
			} else {
				return "none" + rv
			}

		case bool:
			return lv == nil || rv

		case error:
			return lv == nil || rv == nil

		default:
			return errors.New("could not determine type of right side")
		}

	default:
		return errors.New("could not determine type of left side")
	}
}

func (a *exprAdd) String(indentLevel int) string {
	buf := bytes.NewBuffer(nil)

	buf.WriteString(strings.Repeat("\t", indentLevel))
	buf.WriteString("Add:\n")
	buf.WriteString(a.Left.String(indentLevel + 1))
	buf.WriteByte('\n')
	buf.WriteString(a.Right.String(indentLevel + 1))

	return buf.String()
}

type exprSub struct {
	Left  Expr
	Right Expr
}

func (s *exprSub) Eval(e *Env) interface{} {
	lhs := s.Left.Eval(e)
	rhs := s.Right.Eval(e)

	switch lv := lhs.(type) {
	case float64:
		switch rv := rhs.(type) {
		case float64:
			return lv - rv

		default:
			return errors.New("subtraction only defined for numbers")
		}

	default:
		return errors.New("subtraction only defined for numbers")
	}
}

func (s *exprSub) String(indentLevel int) string {
	buf := bytes.NewBuffer(nil)

	buf.WriteString(strings.Repeat("\t", indentLevel))
	buf.WriteString("Subtract:\n")
	buf.WriteString(s.Left.String(indentLevel + 1))
	buf.WriteByte('\n')
	buf.WriteString(s.Right.String(indentLevel + 1))

	return buf.String()
}

type exprMultiply struct {
	Left  Expr
	Right Expr
}

func (m *exprMultiply) Eval(e *Env) interface{} {
	lhs := m.Left.Eval(e)
	rhs := m.Right.Eval(e)

	switch lv := lhs.(type) {
	case float64:
		switch rv := rhs.(type) {
		case float64:
			return lv * rv

		case string:
			return errors.New("cannot multiply number and string")

		case bool:
			return errors.New("cannot multiply number and bool")

		case error:
			return errors.New("cannot multiply number and error")

		default:
			return errors.New("could not determine type of right side")
		}

	case string:
		return errors.New("multiply not defined for string")

	case bool:
		switch rv := rhs.(type) {
		case float64:
			return errors.New("cannot multiply bool and number")

		case string:
			return errors.New("cannot multiply bool and string")

		case bool:
			return lv && rv

		case error:
			return lv && rv == nil

		default:
			return errors.New("could not determine type of right side")
		}

	case error:
		switch rv := rhs.(type) {
		case float64:
			return errors.New("cannot multiply error and number")

		case string:
			return errors.New("cannot multiply error and string")

		case bool:
			return lv == nil && rv

		case error:
			return lv == nil && rv == nil

		default:
			return errors.New("could not determine type of right side")
		}

	default:
		return errors.New("could not determine type of left side")
	}
}

func (m *exprMultiply) String(indentLevel int) string {
	buf := bytes.NewBuffer(nil)

	buf.WriteString(strings.Repeat("\t", indentLevel))
	buf.WriteString("Multiply:\n")
	buf.WriteString(m.Left.String(indentLevel + 1))
	buf.WriteByte('\n')
	buf.WriteString(m.Right.String(indentLevel + 1))

	return buf.String()
}

type exprDivide struct {
	Left  Expr
	Right Expr
}

func (d *exprDivide) Eval(e *Env) interface{} {
	lhs := d.Left.Eval(e)
	rhs := d.Right.Eval(e)

	switch lv := lhs.(type) {
	case float64:
		switch rv := rhs.(type) {
		case float64:
			return lv / rv

		default:
			return errors.New("divide only defined for number")
		}

	default:
		return errors.New("divide only defined for number")
	}
}

func (d *exprDivide) String(indentLevel int) string {
	buf := bytes.NewBuffer(nil)

	buf.WriteString(strings.Repeat("\t", indentLevel))
	buf.WriteString("Divide:\n")
	buf.WriteString(d.Left.String(indentLevel + 1))
	buf.WriteByte('\n')
	buf.WriteString(d.Right.String(indentLevel + 1))

	return buf.String()
}

type exprAssign struct {
	Left  string
	Right Expr
}

func (a *exprAssign) Eval(e *Env) interface{} {
	val := a.Right.Eval(e)
	e.Set(a.Left, val)
	return val
}

func (a *exprAssign) String(indentLevel int) string {
	buf := bytes.NewBuffer(nil)

	buf.WriteString(strings.Repeat("\t", indentLevel))
	buf.WriteString("Assign:\n")
	buf.WriteString(strings.Repeat("\t", indentLevel+1))
	buf.WriteString("Var:")
	buf.WriteString(a.Left)
	buf.WriteByte('\n')
	buf.WriteString(a.Right.String(indentLevel + 1))

	return buf.String()
}
