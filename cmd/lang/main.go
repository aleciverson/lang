package main

import (
	"flag"
	"fmt"
	"os"

	"bitbucket.org/aleciverson/lang/pkg/lang"
)

func main() {
	var (
		printAST bool
	)
	flag.BoolVar(&printAST, "ast", false, "print the AST instead of executing")
	flag.Parse()

	scripts := flag.Args()
	if len(scripts) < 1 {
		fmt.Println("must specify a script to execute")
		os.Exit(1)
	}

	var tokens []lang.Token

	// lex all given scripts
	for _, s := range scripts {
		toks, err := lexScript(s)
		if err != nil {
			fmt.Println("error lexing script " + s + ": " + err.Error())
			os.Exit(1)
		}
		tokens = append(tokens, toks...)
	}

	script, err := lang.Parse(tokens)
	if err != nil {
		fmt.Println("error parsing: " + err.Error())
		os.Exit(1)
	}

	switch {
	case printAST:
		for n, f := range script.Funcs {
			fmt.Printf("%s: %s\n", n, f)
		}

	default:
		ret, err := script.Call("main")
		if err != nil {
			fmt.Println("error executing: " + err.Error())
			os.Exit(1)
		}
		if ret != nil {
			fmt.Println(ret)
		}
	}
}

func lexScript(path string) ([]lang.Token, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return lang.Lex(path, f)
}
